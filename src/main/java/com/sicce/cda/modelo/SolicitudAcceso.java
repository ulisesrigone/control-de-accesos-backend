package com.sicce.cda.modelo;

public class SolicitudAcceso {
    
    private long espacio_id;
    private String macAddress;
    private boolean accede;
    private String mensaje;

    public SolicitudAcceso() {}

    public long getEspacio_id() {
      return espacio_id;
    }

    public void setEspacio_id(long espacio_id) {
      this.espacio_id = espacio_id;
    }

    public boolean isAccede() {
      return accede;
    }

    public void setAccede(boolean accede) {
      this.accede = accede;
    }

    public String getMensaje() {
      return mensaje;
    }

    public void setMensaje(String mensaje) {
      this.mensaje = mensaje;
    }

    public String getMacAddress() {
      return macAddress;
    }

    public void setMacAddress(String macAddress) {
      this.macAddress = macAddress;
    }

    @Override
    public String toString() {
        return "SolicitudAcceso{" + "accede=" + accede + ", mensaje=" + mensaje + ", espacio_id=" + espacio_id + ", madAddress=" + macAddress;
    }
    
}