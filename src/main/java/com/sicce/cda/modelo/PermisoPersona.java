package com.sicce.cda.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PermisoPersona {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO) private long id;
	
	private boolean activo;
		
	@ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;
	
	@ManyToOne
	@JoinColumn(name = "vinculo_momento_espacio_id")
	private VinculoMomentoEspacio vinculoMomentoEspacio;
	
	public PermisoPersona() {
    }
 
	public PermisoPersona(long id) {
    	this.id = id;
    }
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public VinculoMomentoEspacio getVinculoMomentoEspacio() {
		return vinculoMomentoEspacio;
	}

	public void setVinculoMomentoEspacio(VinculoMomentoEspacio vinculoMomentoEspacio) {
		this.vinculoMomentoEspacio = vinculoMomentoEspacio;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
}