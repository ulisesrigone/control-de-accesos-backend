package com.sicce.cda.modelo;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "SolicitudPermiso")
public class SolicitudPermiso {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
    private String macAddress;
    private int aceptado = 0;
    @Basic
	private java.time.LocalDateTime fechaSolicitud;

    @ManyToOne
	@JoinColumn(name="vinculo_momento_espacio_id")
    private VinculoMomentoEspacio vinculoMomentoEspacio;

    @ManyToOne
	@JoinColumn(name="persona_id")
    private Persona persona;

    @ManyToOne
	@JoinColumn(name="permiso_persona_id")
    private PermisoPersona permisoPersona;
    
    public SolicitudPermiso() {}
    public SolicitudPermiso(String id) {
		this.id = Long.parseLong(id);
	}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public int getAceptado() {
        return aceptado;
    }

    public void setAceptado(int aceptado) {
        this.aceptado = aceptado;
    }

    public java.time.LocalDateTime getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(java.time.LocalDateTime fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

    public VinculoMomentoEspacio getVinculoMomentoEspacio() {
        return vinculoMomentoEspacio;
    }

    public void setVinculoMomentoEspacio(VinculoMomentoEspacio vinculoMomentoEspacio) {
        this.vinculoMomentoEspacio = vinculoMomentoEspacio;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public PermisoPersona getPermisoPersona() {
        return permisoPersona;
    }

    public void setPermisoPersona(PermisoPersona permisoPersona) {
        this.permisoPersona = permisoPersona;
    }

    @Override
    public String toString() {
        return "SolicitudPermiso { MACAddress=" + macAddress + ", vinculo_momento_espacio_id=" + vinculoMomentoEspacio + " }";
    }

}
    
