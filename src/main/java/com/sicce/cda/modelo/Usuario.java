package com.sicce.cda.modelo;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity(name = "Usuario")

public class Usuario {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
	private static final long serialVersionUID = 1L;
	private String nombreUsuario;
	private String clave;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(unique = true)
	private Persona persona;
	
	@ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
        	})
	@JoinTable(
			name = "Usuario_EspacioControlado", 
			joinColumns = { @JoinColumn(name = "usuario_id") }, 
			inverseJoinColumns = { @JoinColumn(name = "espacioControlado_id") }
			)
	Set<Espacio> espaciosControlados = new HashSet<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Set<Espacio> getEspaciosControlados() {
		return espaciosControlados;
	}

	public void setEspaciosControlados(Set<Espacio> espaciosControlados) {
		this.espaciosControlados = espaciosControlados;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}		
}
