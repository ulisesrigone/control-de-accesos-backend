package com.sicce.cda.modelo;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Momento {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
	private java.time.DayOfWeek diaDeSemana;
	@Basic
	private java.time.LocalTime horarioInicio;
	@Basic
	private java.time.LocalTime horarioFin;
	private boolean activo;
	
	public Momento() {}
	
	public Momento(String id) {
		this.id = Long.parseLong(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.time.DayOfWeek getDiaDeSemana() {
		return diaDeSemana;
	}

	public void setDiaDeSemana(java.time.DayOfWeek diaDeSemana) {
		this.diaDeSemana = diaDeSemana;
	}

	public java.time.LocalTime getHorarioInicio() {
		return horarioInicio;
	}

	public void setHorarioInicio(java.time.LocalTime horarioInicio) {
		this.horarioInicio = horarioInicio;
	}

	public java.time.LocalTime getHorarioFin() {
		return horarioFin;
	}

	public void setHorarioFin(java.time.LocalTime horarioFin) {
		this.horarioFin = horarioFin;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/*
	public Set<Espacio> getEspacios() {
		return espacios;
	}

	public void setEspacios(Set<Espacio> espacios) {
		this.espacios = espacios;
	}
	*/

	/*
	public Set<PermisoPersona> getPermisosPersona() {
        return permisosPersona;
    }

    public void setPermisosPersona(Set<PermisoPersona> permisosPersona) {
        this.permisosPersona = permisosPersona;
    }
    */

    @Override
    public String toString() {
        String result = String.format("%d", id);
        /*
        if (permisosPersona != null) {
            for(PermisoPersona permisoPersona : permisosPersona) {
                result += String.format(
                        "PermisoPersona[id=%d]%n",
                        permisoPersona.getId());
            }
        }
        */

        return result;
    }

	
}