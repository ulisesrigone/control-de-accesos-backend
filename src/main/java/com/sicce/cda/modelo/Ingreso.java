package com.sicce.cda.modelo;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Ingreso {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
	@Basic
	private java.time.LocalDateTime fechaIngreso;
	
	@ManyToOne
	@JoinColumn(name="persona_id", nullable=false)
	private Persona persona;
	
	@ManyToOne
	@JoinColumn(name="espacio_id", nullable=false)
	private Espacio espacio;

	public Ingreso() {}
	public Ingreso(String id) {
		this.id = Long.parseLong(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.time.LocalDateTime getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(java.time.LocalDateTime fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Espacio getEspacio() {
		return espacio;
	}

	public void setEspacio(Espacio espacio) {
		this.espacio = espacio;
	}

}
