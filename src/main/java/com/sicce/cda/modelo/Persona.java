package com.sicce.cda.modelo;

//import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.OneToMany;

@Entity(name = "Persona")
 
public class Persona {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
	private static final long serialVersionUID = 1L;
	private String macAddress;
	private String nombre;
	private String apellidos;
	private String celular;
	private String email;
	
	public Persona() {}
	public Persona(String id) {
		this.id = Long.parseLong(id);
	}
	
	public long getId() {
		return id;                                                                                                                                                                                                                                                                                                                                                                                                                        
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nombre=" + nombre + ", email=" + email + '}';
    }
}