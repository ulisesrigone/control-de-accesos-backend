package com.sicce.cda.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class VinculoMomentoEspacio {
    
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;

	@ManyToOne
    @JoinColumn(name = "espacio_id")
    private Espacio espacio;

    @ManyToOne
    @JoinColumn(name = "momento_id")
    private Momento momento;

    public VinculoMomentoEspacio() {
    }
 
	public VinculoMomentoEspacio(long id) {
    	this.id = id;
    }
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
    }
    
    public Espacio getEspacio() {
		return espacio;
	}

	public void setEspacio(Espacio espacio) {
		this.espacio = espacio;
    }
    
    public Momento getMomento() {
		return momento;
	}

	public void setMomento(Momento momento) {
		this.momento = momento;
	}

}