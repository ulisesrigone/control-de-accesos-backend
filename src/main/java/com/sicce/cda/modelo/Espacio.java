package com.sicce.cda.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Espacio {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
	private String nombre;
	private String descripcion;
	private boolean activo = true;

	public Espacio() {}
	public Espacio(String id) {
		this.id = Long.parseLong(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public String toString() {
    	return "Espacio [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + "]";
    }
}