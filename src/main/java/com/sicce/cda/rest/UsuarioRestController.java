package com.sicce.cda.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.sicce.cda.modelo.Usuario;
import com.sicce.cda.service.UsuarioService;

@RestController /* para enviar y recibir JSON */ 
public class UsuarioRestController {
	
	@Autowired
	private UsuarioService usuarioService;
	
// Devolver un usuario -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> getUsuario(@PathVariable("id") long id) {
        System.out.println("Usuario con id=" + id);
        Usuario usuario = usuarioService.findById(id);
        if (usuario == null) {
            System.out.println("Usuario con id=" + id + " no existe.");
            return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
}

 // Crear un usuario -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/usuario/", method = RequestMethod.POST)
    public ResponseEntity<Void> createUsuario(@RequestBody Usuario usuario,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creando usuario " + usuario.getNombreUsuario());
 
        if (usuarioService.isUsuarioExist(usuario)) {
            System.out.println("La usuario con usuario=" + usuario.getNombreUsuario() + " ya existe.");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
 
        usuarioService.saveUsuario(usuario);
        
        //Esto es para generar el id de la nueva usuario
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/usuario/{id}").buildAndExpand(usuario.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
	
}