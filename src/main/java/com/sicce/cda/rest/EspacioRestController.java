package com.sicce.cda.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.sicce.cda.modelo.Espacio;
import com.sicce.cda.modelo.Ingreso;
import com.sicce.cda.service.EspacioService;
import com.sicce.cda.service.IngresoService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EspacioRestController {
	
	@Autowired
    private EspacioService espacioService;
    @Autowired
    private IngresoService ingresoService;
	
// Devolver un espacio -------------------------------------------------------------------------------------------------
    
	
    @RequestMapping(value = "/espacios/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Espacio> getEspacio(@PathVariable("id") long id) {
        Espacio espacio = espacioService.findById(id);
        if (espacio == null) {
            System.out.println("Espacio con id=" + id + " no existe.");
            return new ResponseEntity<Espacio>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Espacio>(espacio, HttpStatus.OK);
	}

// Devolver todos los espacios -------------------------------------------------------------------------------------------------

    @GetMapping("/espacios/")
    public List<Espacio> getEspacios() {
        return (List<Espacio>) espacioService.findAllEspacios();
    }
    
// Crear un espacio -------------------------------------------------------------------------------------------------
    
    @PostMapping("/espacios/")
    void addEspacio(@RequestBody Espacio espacio) {
        espacioService.saveEspacio(espacio);
    }

// Actualizar un espacio --------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/espacio/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Espacio> updateEspacio(@PathVariable("id") long id, @RequestBody Espacio espacio) {
        System.out.println("Actualizando espacio con id: " + id);
         
        Espacio currentEspacio = espacioService.findById(id);
         
        if (currentEspacio==null) {
            System.out.println("Espacio con id:" + id + " no existe.");
            return new ResponseEntity<Espacio>(HttpStatus.NOT_FOUND);
        }
 
        currentEspacio.setNombre(espacio.getNombre());
        currentEspacio.setDescripcion(espacio.getDescripcion());
       
        espacioService.updateEspacio(currentEspacio);
        return new ResponseEntity<Espacio>(currentEspacio, HttpStatus.OK);
    }
    
// Eliminar un espacio -----------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/espacio/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Espacio> deleteEspacio(@PathVariable("id") long id) {
        System.out.println("Eliminando espacio con id: " + id);
 
        Espacio espacio = espacioService.findById(id);
        if (espacio == null) {
            System.out.println("No se pudo eliminar. La espacio con id: " + id + " no existe.");
            return new ResponseEntity<Espacio>(HttpStatus.NOT_FOUND);
        }
 
        espacioService.deleteEspacioById(id);
        return new ResponseEntity<Espacio>(HttpStatus.NO_CONTENT);
    }
    
// Eliminar todos los espacios --------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/espacio/", method = RequestMethod.DELETE)
    public ResponseEntity<Espacio> deleteAllEspacios() {
        System.out.println("Eliminando todas las espacios.");
 
        espacioService.deleteAllEspacios();
        return new ResponseEntity<Espacio>(HttpStatus.NO_CONTENT);
    }

// Devolver Horarios de un Espacio ----------------------------------------------------------------------------------------

    @GetMapping("espacio/{idEspacio}/ingresos/")
    public ResponseEntity<List<Ingreso>> getIngresosByEspacio(@PathVariable("idEspacio") long idEspacio) {
        List<Ingreso> ingresos = ingresoService.findByEspacio(idEspacio);
        if(ingresos.isEmpty()){
            System.out.println("No hay horarios disponibles para Espacio: " + idEspacio);
            return new ResponseEntity<List<Ingreso>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Ingreso>>(ingresos, HttpStatus.OK);
    }

}

