package com.sicce.cda.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.sicce.cda.modelo.VinculoMomentoEspacio;
import com.sicce.cda.service.VinculoMomentoEspacioService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class VinculoMomentoEspacioRestController {
	
	@Autowired
	VinculoMomentoEspacioService vinculoMomentoEspacioService;
	
// Devolver un vinculoMomentoEspacio -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/horarios/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VinculoMomentoEspacio> getVinculoMomentoEspacio(@PathVariable("id") long id) {
        System.out.println("VinculoMomentoEspacio con id=" + id);
        VinculoMomentoEspacio vinculoMomentoEspacio = vinculoMomentoEspacioService.findById(id);
        if (vinculoMomentoEspacio == null) {
            System.out.println("VinculoMomentoEspacio con id=" + id + " no existe.");
            return new ResponseEntity<VinculoMomentoEspacio>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<VinculoMomentoEspacio>(vinculoMomentoEspacio, HttpStatus.OK);
}

// Devolver todos los vinculoMomentoEspacios -------------------------------------------------------------------------------------------------

@GetMapping("/horarios/")
public List<VinculoMomentoEspacio> getVinculoMomentoEspacios() {
    return (List<VinculoMomentoEspacio>) vinculoMomentoEspacioService.findAllVinculoMomentoEspacios();
}

// Devolver Horarios de un Espacio ----------------------------------------------------------------------------------------

    @GetMapping("espacio/{idEspacio}/horarios/")
    public ResponseEntity<List<VinculoMomentoEspacio>> getHorariosByEspacio(@PathVariable("idEspacio") long idEspacio) {
        List<VinculoMomentoEspacio> vinculoMomentoEspacios = vinculoMomentoEspacioService.findByEspacio(idEspacio);
        if(vinculoMomentoEspacios.isEmpty()){
            System.out.println("No hay horarios disponibles para Espacio: " + idEspacio);
            return new ResponseEntity<List<VinculoMomentoEspacio>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<VinculoMomentoEspacio>>(vinculoMomentoEspacios, HttpStatus.OK);
    }
    
// Crear un vinculoMomentoEspacio -------------------------------------------------------------------------------------------------
    
    @PostMapping("/horarios/")
    void addEspacio(@RequestBody VinculoMomentoEspacio vinculoMomentoEspacio) {
        vinculoMomentoEspacioService.saveVinculoMomentoEspacio(vinculoMomentoEspacio);
    }

// Actualizar un vinculoMomentoEspacio --------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/horarios/{id}", method = RequestMethod.PUT)
    public ResponseEntity<VinculoMomentoEspacio> updateVinculoMomentoEspacio(@PathVariable("id") long id, @RequestBody VinculoMomentoEspacio vinculoMomentoEspacio) {
        System.out.println("Actualizando vinculoMomentoEspacio con id: " + id);
         
        VinculoMomentoEspacio currentVinculoMomentoEspacio = vinculoMomentoEspacioService.findById(id);
         
        if (currentVinculoMomentoEspacio==null) {
            System.out.println("VinculoMomentoEspacio con id:" + id + " no existe.");
            return new ResponseEntity<VinculoMomentoEspacio>(HttpStatus.NOT_FOUND);
        }
 
        currentVinculoMomentoEspacio.setId(vinculoMomentoEspacio.getId());
       
        vinculoMomentoEspacioService.updateVinculoMomentoEspacio(currentVinculoMomentoEspacio);
        return new ResponseEntity<VinculoMomentoEspacio>(currentVinculoMomentoEspacio, HttpStatus.OK);
    }
    
// Eliminar un vinculoMomentoEspacio -----------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/horarios/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<VinculoMomentoEspacio> deleteVinculoMomentoEspacio(@PathVariable("id") long id) {
        System.out.println("Eliminando vinculoMomentoEspacio con id: " + id);
 
        VinculoMomentoEspacio vinculoMomentoEspacio = vinculoMomentoEspacioService.findById(id);
        if (vinculoMomentoEspacio == null) {
            System.out.println("No se pudo eliminar. La vinculoMomentoEspacio con id: " + id + " no existe.");
            return new ResponseEntity<VinculoMomentoEspacio>(HttpStatus.NOT_FOUND);
        }
 
        vinculoMomentoEspacioService.deleteVinculoMomentoEspacioById(id);
        return new ResponseEntity<VinculoMomentoEspacio>(HttpStatus.NO_CONTENT);
    }
    
// Eliminar todos los vinculoMomentoEspacios --------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/horarios/", method = RequestMethod.DELETE)
    public ResponseEntity<VinculoMomentoEspacio> deleteAllVinculoMomentoEspacios() {
        System.out.println("Eliminando todas las vinculoMomentoEspacios.");
 
        vinculoMomentoEspacioService.deleteAllVinculoMomentoEspacios();
        return new ResponseEntity<VinculoMomentoEspacio>(HttpStatus.NO_CONTENT);
    }
}