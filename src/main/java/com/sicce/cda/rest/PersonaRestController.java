package com.sicce.cda.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.sicce.cda.modelo.Persona;
import com.sicce.cda.service.PersonaService;

@RestController /* para enviar y recibir JSON */
@CrossOrigin(origins = "http://localhost:4200")
public class PersonaRestController {
	
	@Autowired
	private PersonaService personaService;
	
	@CrossOrigin(origins = "http://localhost:4200")

	
// Devolver una persona -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/persona/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Persona> getPersona(@PathVariable("id") long id) {
        System.out.println("Persona con id=" + id);
        Persona persona = personaService.findById(id);
        if (persona == null) {
            System.out.println("Persona con id=" + id + " no existe.");
            return new ResponseEntity<Persona>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Persona>(persona, HttpStatus.OK);
}

    @GetMapping("/persona/")
    public List<Persona> getPersonas() {
        return (List<Persona>) personaService.findAllPersonas();
    }
    
// Crear una persona -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/persona/", method = RequestMethod.POST)
    public ResponseEntity<Void> createPersona(@RequestBody Persona persona,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creando persona " + persona.getApellidos() + ", " + persona.getNombre() + " ...");
 
        if (personaService.isPersonaExist(persona)) {
            System.out.println("La persona con email=" + persona.getEmail() + " ya existe.");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
 
        personaService.savePersona(persona);
        
        // Esto es para generar el id de la nueva persona
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/persona/{id}").buildAndExpand(persona.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
	    
// Actualizar una persona --------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/persona/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Persona> updatePersona(@PathVariable("id") long id, @RequestBody Persona persona) {
        System.out.println("Actualizando persona con id: " + id);
         
        Persona currentPersona = personaService.findById(id);
         
        if (currentPersona==null) {
            System.out.println("Persona con id:" + id + " no existe.");
            return new ResponseEntity<Persona>(HttpStatus.NOT_FOUND);
        }
 
        currentPersona.setNombre(persona.getNombre());
        currentPersona.setEmail(persona.getEmail());
        currentPersona.setCelular(persona.getCelular());
        currentPersona.setApellidos(persona.getApellidos());
         
        personaService.updatePersona(currentPersona);
        return new ResponseEntity<Persona>(currentPersona, HttpStatus.OK);
    }
    
// Eliminar una persona -----------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/persona/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Persona> deletePersona(@PathVariable("id") long id) {
        System.out.println("Eliminando persona con id: " + id);
 
        Persona persona = personaService.findById(id);
        if (persona == null) {
            System.out.println("No se pudo eliminar. La persona con id: " + id + " no existe.");
            return new ResponseEntity<Persona>(HttpStatus.NOT_FOUND);
        }
 
        personaService.deletePersonaById(id);
        return new ResponseEntity<Persona>(HttpStatus.NO_CONTENT);
    }
    
// Eliminar todas las personas --------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/persona/", method = RequestMethod.DELETE)
    public ResponseEntity<Persona> deleteAllPersonas() {
        System.out.println("Eliminando todas las personas.");
 
        personaService.deleteAllPersonas();
        return new ResponseEntity<Persona>(HttpStatus.NO_CONTENT);
    }
    
}

