package com.sicce.cda.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.sicce.cda.modelo.Momento;
import com.sicce.cda.service.MomentoService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MomentoRestController {

	@Autowired
	private MomentoService momentoService;
	
// Devolver un momento -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/momento/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Momento> getMomento(@PathVariable("id") long id) {
        System.out.println("Momento con id=" + id);
        Momento momento = momentoService.findById(id);
        if (momento == null) {
            System.out.println("Momento con id=" + id + " no existe.");
            return new ResponseEntity<Momento>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Momento>(momento, HttpStatus.OK);
}

// Devolver todas las momentos -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/momento/", method = RequestMethod.GET)
    public ResponseEntity<List<Momento>> listAllMomentos() {
        List<Momento> momentos = momentoService.findAllMomentos();
        if(momentos.isEmpty()){
            return new ResponseEntity<List<Momento>>(HttpStatus.NO_CONTENT); //You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Momento>>(momentos, HttpStatus.OK);
    }    

// Crear una momento -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/momento/", method = RequestMethod.POST)
    public ResponseEntity<Void> createMomento(@RequestBody Momento momento,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creando momento con id=" + momento.getId() + " ...");
 
        if (momentoService.isMomentoExist(momento)) {
            System.out.println("La momento con id=" + momento.getId() + " ya existe.");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
 
        momentoService.saveMomento(momento);
        
        // Esto es para generar el id de la nueva momento
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/momento/{id}").buildAndExpand(momento.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

// Actualizar una momento --------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/momento/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Momento> updateMomento(@PathVariable("id") long id, @RequestBody Momento momento) {
        System.out.println("Actualizando momento con id: " + id);
         
        Momento currentMomento = momentoService.findById(id);
         
        if (currentMomento==null) {
            System.out.println("Momento con id:" + id + " no existe.");
            return new ResponseEntity<Momento>(HttpStatus.NOT_FOUND);
        }
 
        currentMomento.setDiaDeSemana(momento.getDiaDeSemana());
        currentMomento.setHorarioInicio(momento.getHorarioInicio());
        currentMomento.setHorarioFin(momento.getHorarioFin());
        currentMomento.setActivo(momento.isActivo());
         
        momentoService.updateMomento(currentMomento);
        return new ResponseEntity<Momento>(currentMomento, HttpStatus.OK);
    }
    
// Eliminar una momento -----------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/momento/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Momento> deleteMomento(@PathVariable("id") long id) {
        System.out.println("Eliminando momento con id: " + id);
 
        Momento momento = momentoService.findById(id);
        if (momento == null) {
            System.out.println("No se pudo eliminar. La momento con id: " + id + " no existe.");
            return new ResponseEntity<Momento>(HttpStatus.NOT_FOUND);
        }
 
        momentoService.deleteMomentoById(id);
        return new ResponseEntity<Momento>(HttpStatus.NO_CONTENT);
    }
    
// Eliminar todas las momentos --------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/momento/", method = RequestMethod.DELETE)
    public ResponseEntity<Momento> deleteAllMomentos() {
        System.out.println("Eliminando todas las momentos.");
 
        momentoService.deleteAllMomentos();
        return new ResponseEntity<Momento>(HttpStatus.NO_CONTENT);
    }

// Crear HORARIO para un espacio -------------------------------------------------------------------------

    @PostMapping("/espacio/{idEspacio}/horarios/")
    void addHorario(@PathVariable("idEspacio") long idEspacio, @RequestBody Momento momento) {
        momentoService.saveHorarioByEspacio(idEspacio, momento);
    }
	
}
