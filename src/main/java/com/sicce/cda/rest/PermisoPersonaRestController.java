package com.sicce.cda.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.sicce.cda.modelo.PermisoPersona;
import com.sicce.cda.modelo.SolicitudPermiso;
import com.sicce.cda.service.PermisoPersonaService;
import com.sicce.cda.service.SolicitudPermisoService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")

public class PermisoPersonaRestController {
	
    @Autowired
    PermisoPersonaService permisoPersonaService;
    @Autowired
    SolicitudPermisoService solicitudPermisoService;
	
// Devolver un permisoPersona -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/permisos/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PermisoPersona> getPermisoPersona(@PathVariable("id") long id) {
        System.out.println("PermisoPersona con id=" + id);
        PermisoPersona permisoPersona = permisoPersonaService.findById(id);
        if (permisoPersona == null) {
            System.out.println("PermisoPersona con id=" + id + " no existe.");
            return new ResponseEntity<PermisoPersona>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<PermisoPersona>(permisoPersona, HttpStatus.OK);
}

// Devolver todos los permisoPersonas -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/permisos/", method = RequestMethod.GET)
    public ResponseEntity<List<PermisoPersona>> listAllPermisoPersonas() {
        List<PermisoPersona> permisosPersonas = permisoPersonaService.findAllPermisosPersonas();
        if(permisosPersonas.isEmpty()){
            return new ResponseEntity<List<PermisoPersona>>(HttpStatus.NO_CONTENT); //You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<PermisoPersona>>(permisosPersonas, HttpStatus.OK);
    }

// Devolver permisos de un Espacio ----------------------------------------------------------------------------------------

    @GetMapping("espacio/{idEspacio}/permisos/")
    public ResponseEntity<List<PermisoPersona>> getPermisosByEspacio(@PathVariable("idEspacio") long idEspacio) {
        List<PermisoPersona> permisosPersonas = permisoPersonaService.findByEspacio(idEspacio);
        if(permisosPersonas.isEmpty()){
            System.out.println("No hay permisos para Espacio: " + idEspacio);
            return new ResponseEntity<List<PermisoPersona>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<PermisoPersona>>(permisosPersonas, HttpStatus.OK);
    }
    
// Crear un permisoPersona -------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/permiso/", method = RequestMethod.POST)
    public ResponseEntity<Void> createPermisoPersona(@RequestBody PermisoPersona permisoPersona,    UriComponentsBuilder ucBuilder) {
    	System.out.println("PermisoPersona: Activo=" + permisoPersona.isActivo() + permisoPersona.getPersona() + "Momento " + permisoPersona.getVinculoMomentoEspacio().getMomento().getHorarioFin());
        		
        if (permisoPersonaService.isPermisoPersonaExist(permisoPersona)) {
            System.out.println("La permisoPersona con id=" + permisoPersona.getId() + " ya existe.");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        permisoPersonaService.savePermisoPersona(permisoPersona);
        
        // Esto es para generar el id
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/permisoPersona/{id}").buildAndExpand(permisoPersona.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PostMapping("/permisos/")
    void addPermisoPersona(@RequestBody PermisoPersona permisoPersona) {
        System.out.println("PermisoPersona: Activo=" + permisoPersona.isActivo() + permisoPersona.getPersona() + "Momento " + permisoPersona.getVinculoMomentoEspacio().getMomento().getHorarioFin());
        permisoPersonaService.savePermisoPersona(permisoPersona);
    }
	    
// Actualizar un permisoPersona --------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/permiso/{id}", method = RequestMethod.PUT)
    public ResponseEntity<PermisoPersona> updatePermisoPersona(@PathVariable("id") long id, @RequestBody PermisoPersona permisoPersona) {
        System.out.println("Actualizando permisoPersona con id: " + id);
         
        PermisoPersona currentPermisoPersona = permisoPersonaService.findById(id);
         
        if (currentPermisoPersona==null) {
            System.out.println("PermisoPersona con id:" + id + " no existe.");
            return new ResponseEntity<PermisoPersona>(HttpStatus.NOT_FOUND);
        }
 
        currentPermisoPersona.setId(permisoPersona.getId());
       
        permisoPersonaService.updatePermisoPersona(currentPermisoPersona);
        return new ResponseEntity<PermisoPersona>(currentPermisoPersona, HttpStatus.OK);
    }
    
// Eliminar un permisoPersona -----------------------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/permiso/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<PermisoPersona> deletePermisoPersona(@PathVariable("id") long id) {
        System.out.println("Eliminando permisoPersona con id: " + id);
 
        PermisoPersona permisoPersona = permisoPersonaService.findById(id);
        if (permisoPersona == null) {
            System.out.println("No se pudo eliminar. La permisoPersona con id: " + id + " no existe.");
            return new ResponseEntity<PermisoPersona>(HttpStatus.NOT_FOUND);
        }
 
        permisoPersonaService.deletePermisoPersonaById(id);
        return new ResponseEntity<PermisoPersona>(HttpStatus.NO_CONTENT);
    }
    
// Eliminar todos los permisoPersonas --------------------------------------------------------------------------------------
    
    @RequestMapping(value = "/permisos/", method = RequestMethod.DELETE)
    public ResponseEntity<PermisoPersona> deleteAllPermisoPersonas() {
        System.out.println("Eliminando todas las permisoPersonas.");
 
        permisoPersonaService.deleteAllPermisoPersonas();
        return new ResponseEntity<PermisoPersona>(HttpStatus.NO_CONTENT);
    }

// Devolver todas las solicitudes de permisos ------------------------------------------------------    
    
    @GetMapping("/permisos/solicitudes/")
    public List<SolicitudPermiso> getSolicitudPermisos() {
        return (List<SolicitudPermiso>) solicitudPermisoService.findAllSolicitudPermisos();
    }

// Devolver las solicitudes de permisos de UN ESPACIO -------------------------------------------------

    @GetMapping("espacio/{idEspacio}/solicitudes/")
    public ResponseEntity<List<SolicitudPermiso>> getSolicitudesByEspacio(@PathVariable("idEspacio") long idEspacio) {
        List<SolicitudPermiso> solicitudesPermisos = solicitudPermisoService.findByEspacio(idEspacio);
        return new ResponseEntity<List<SolicitudPermiso>>(solicitudesPermisos, HttpStatus.OK);
    }

// Actualizar solicitud de permiso (ACEPTAR/RECHAZAR) -------------------------------------------------

    @PutMapping("/solicitud/")
    public void updateSolicitudPermiso(@RequestBody SolicitudPermiso solicitudPermiso) {
        solicitudPermisoService.respuestaSolicitudPermiso(solicitudPermiso);
    }
}
