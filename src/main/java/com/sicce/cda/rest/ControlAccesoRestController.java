package com.sicce.cda.rest;

import java.util.List;
import com.sicce.cda.modelo.Persona;
import com.sicce.cda.modelo.SolicitudAcceso;
import com.sicce.cda.modelo.SolicitudPermiso;
import com.sicce.cda.modelo.VinculoMomentoEspacio;
import com.sicce.cda.service.IngresoService;
import com.sicce.cda.service.PermisoPersonaService;
import com.sicce.cda.service.PersonaService;
import com.sicce.cda.service.SolicitudPermisoService;
import com.sicce.cda.service.VinculoMomentoEspacioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
//@CrossOrigin( (Origin de hardware controlador RFID), (URI de App Móvil) )

public class ControlAccesoRestController {
    
    private VinculoMomentoEspacioService vinculoMomentoEspacioService;

    @Autowired
    SolicitudPermisoService solicitudPermisoService;

    @Autowired
    PersonaService personaService;

    @Autowired
    PermisoPersonaService permisoPersonaService;

    @Autowired
    IngresoService ingresoService;
    
    @Autowired
    public void setVinculoMomentoEspacioService(VinculoMomentoEspacioService vinculoMomentoEspacioService) {
        this.vinculoMomentoEspacioService = vinculoMomentoEspacioService;
    }

/* Alta de PERSONA desde App Móvil al iniciarse en la misma. Datos proporcionados como "MACAddress" son indispensables para las validaciones de acceso a un espacio. */

@PostMapping("/externo/persona")
public ResponseEntity<Void> crearPersona(@RequestBody Persona persona) {
    personaService.savePersona(persona);
    return new ResponseEntity<Void>(HttpStatus.CREATED);
}

/* Solicitud desde App Móvil de todos los ESPACIOS y sus correspondientes HORARIOS semanales disponibles. Devuelve conjunto de datos: (Espacios + Horarios de cada uno). */

@GetMapping("/externo/espacios")
public List<VinculoMomentoEspacio> getVinculoMomentoEspacios() {
    return (List<VinculoMomentoEspacio>) vinculoMomentoEspacioService.findAllVinculoMomentoEspacios();
}

/* Alta desde App Móvil de solicitud de PERMISO para acceder a un Horario semanal disponible de un Espacio.
Se guarda el requerimiento a la espera de la aceptación (o no) por parte de un usuario controlador de dicho espacio desde la Aplicación Web de Control de Accesos. */

@PostMapping("/externo/permiso")
public ResponseEntity<Void> solicitaPermiso(@RequestBody SolicitudPermiso solicitudPermiso) {
    solicitudPermisoService.saveSolicitudPermiso(solicitudPermiso);
    return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
}

/* Solicitud de ACCESO a un Espacio desde llave RFID o App Móvil (validación por MACAddress).
Devuelve accede=true/false incluido en el JSON un mensaje de estado. */

@PostMapping("/acceso")
public ResponseEntity<SolicitudAcceso> solicitaAcceso(@RequestBody SolicitudAcceso solicitudAcceso) {
    permisoPersonaService.puedeIngresar(solicitudAcceso);
    return new ResponseEntity<SolicitudAcceso>(solicitudAcceso, HttpStatus.OK);
}
}