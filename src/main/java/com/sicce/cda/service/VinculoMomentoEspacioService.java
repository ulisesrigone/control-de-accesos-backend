package com.sicce.cda.service;

import java.util.List;

import com.sicce.cda.modelo.VinculoMomentoEspacio;

public interface VinculoMomentoEspacioService {

	VinculoMomentoEspacio findById(long id);
	void saveVinculoMomentoEspacio(VinculoMomentoEspacio vinculoMomentoEspacio);
	void updateVinculoMomentoEspacio(VinculoMomentoEspacio vinculoMomentoEspacio);
	void deleteVinculoMomentoEspacioById(long id);
	List<VinculoMomentoEspacio> findAllVinculoMomentoEspacios();
	void deleteAllVinculoMomentoEspacios();
	public boolean isVinculoMomentoEspacioExist(VinculoMomentoEspacio vinculoMomentoEspacio);
	List<VinculoMomentoEspacio> findByEspacio(long idEspacio);
    
}