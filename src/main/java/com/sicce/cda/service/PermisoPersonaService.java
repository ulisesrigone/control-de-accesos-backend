package com.sicce.cda.service;

import java.util.List;

import com.sicce.cda.modelo.PermisoPersona;
import com.sicce.cda.modelo.SolicitudAcceso;

public interface PermisoPersonaService {
	
	PermisoPersona findById(long id);
	List<PermisoPersona> findByEspacio(long id);
	PermisoPersona findByPersona(long id);
	void savePermisoPersona(PermisoPersona permisoPersona);
	void updatePermisoPersona(PermisoPersona permisoPersona);
	void deletePermisoPersonaById(long id);
	List<PermisoPersona> findAllPermisosPersonas();
	void deleteAllPermisoPersonas();
	public boolean isPermisoPersonaExist(PermisoPersona permisoPersona);
	public SolicitudAcceso puedeIngresar(SolicitudAcceso solicitudAcceso);

}
