package com.sicce.cda.service.impl;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.PermisoPersona;
import com.sicce.cda.modelo.Persona;
import com.sicce.cda.modelo.Momento;
import com.sicce.cda.modelo.SolicitudAcceso;
import com.sicce.cda.repository.MomentoRepository;
import com.sicce.cda.repository.PermisoPersonaRepository;
import com.sicce.cda.repository.PersonaRepository;
import com.sicce.cda.service.IngresoService;
import com.sicce.cda.service.PermisoPersonaService;

@Service
public class PermisoPersonaServiceImpl implements PermisoPersonaService {
	
	private PersonaRepository personaRepository;
	private MomentoRepository momentoRepository;
	private IngresoService ingresoService;

	@Autowired
	private PermisoPersonaRepository permisoPersonaRepository;

	@Autowired
    public void setPersonaRepository(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
	}
	
	@Autowired
    public void setMomentoRepository(MomentoRepository momentoRepository) {
        this.momentoRepository = momentoRepository;
	}
	
	@Autowired
    public void setIngresoService(IngresoService ingresoService) {
        this.ingresoService = ingresoService;
	}

	public PermisoPersona findById(long id) {
		return permisoPersonaRepository.findById(id);
	}
	
	public List<PermisoPersona> findAllPermisosPersonas() {
		return (List<PermisoPersona>) permisoPersonaRepository.findAll();
	}
	
	public List<PermisoPersona> findByEspacio(long id) {
		return (List<PermisoPersona>) permisoPersonaRepository.findByEspacio(id);
	}
	
	public PermisoPersona findByPersona(long id) {
		return permisoPersonaRepository.findByPersona(id);
	}

	public void savePermisoPersona(PermisoPersona permisoPersona) {
		permisoPersonaRepository.save(permisoPersona);
	}

	public void updatePermisoPersona(PermisoPersona permisoPersona) {
		permisoPersonaRepository.save(permisoPersona);
	}

	public void deletePermisoPersonaById(long id) {
		permisoPersonaRepository.deleteById(id);
	}

	public void deleteAllPermisoPersonas() {
		permisoPersonaRepository.deleteAll();
	}

	public boolean isPermisoPersonaExist(PermisoPersona permisoPersona) {
		return findById(permisoPersona.getId())!=null;
	}

	public SolicitudAcceso puedeIngresar(SolicitudAcceso solicitudAcceso) {
		solicitudAcceso.setAccede(false);
		try {
			// Se obtienen día de semana y horario actual:
			LocalDateTime fechaActual = LocalDateTime.now();
			DayOfWeek diaDeSemanaActual = fechaActual.getDayOfWeek();
			LocalTime tiempoActual = fechaActual.toLocalTime();
			// Se busca la persona por MACAddress:
			Persona persona = personaRepository.findByMacAddress(solicitudAcceso.getMacAddress());
			// Se obtienen los horarios semanales filtrados por los permisos de la persona:
			List<Momento> horarios = momentoRepository.findByEspacioAndPersona(solicitudAcceso.getEspacio_id(),persona.getId());
			if (horarios.isEmpty()) {
				solicitudAcceso.setMensaje("No autorizado en este espacio.");
			} else {
				// Se compara el horario actual con los horarios filtrados anteriormente:
				solicitudAcceso.setMensaje("No está abierto el espacio.");
				for (Momento horario : horarios) {
					int comparacionInicio = tiempoActual.compareTo(horario.getHorarioInicio());
					int comparacionFin = tiempoActual.compareTo(horario.getHorarioFin());
					if (diaDeSemanaActual == horario.getDiaDeSemana() && comparacionInicio > 0 && comparacionFin < 0) {
						// Se acepta el ingreso.
						solicitudAcceso.setMensaje("Puede ingresar.");
						solicitudAcceso.setAccede(true);
						// Se registra el ingreso.
						ingresoService.saveIngreso(solicitudAcceso.getEspacio_id(), persona.getId(), fechaActual);
						break;
					}
				}
			}
		} catch(NullPointerException e) {
			// La dirección MACAddress no fue reconocida en el sistema.
			solicitudAcceso.setMensaje("Persona no registrada.");
		}
		return solicitudAcceso;
	}
}