package com.sicce.cda.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.PermisoPersona;
import com.sicce.cda.modelo.Persona;
import com.sicce.cda.modelo.SolicitudPermiso;
import com.sicce.cda.repository.PermisoPersonaRepository;
import com.sicce.cda.repository.PersonaRepository;
import com.sicce.cda.repository.SolicitudPermisoRepository;
import com.sicce.cda.service.SolicitudPermisoService;

@Service
public class SolicitudPermisoServiceImpl implements SolicitudPermisoService {

	private PersonaRepository personaRepository;
	private PermisoPersonaRepository permisoPersonaRepository;

	@Autowired
	private SolicitudPermisoRepository solicitudPermisoRepository;
	@Autowired
    public void setPersonaRepository(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
	}
	@Autowired
    public void setPermisoPersonaRepository(PermisoPersonaRepository permisoPersonaRepository) {
        this.permisoPersonaRepository = permisoPersonaRepository;
	}
	
	public void saveSolicitudPermiso(SolicitudPermiso solicitudPermiso) {
		Persona persona = personaRepository.findByMacAddress(solicitudPermiso.getMacAddress());
		solicitudPermiso.setPersona(persona);
		solicitudPermiso.setFechaSolicitud(LocalDateTime.now());
		solicitudPermisoRepository.save(solicitudPermiso);
	}
	
	public void respuestaSolicitudPermiso(SolicitudPermiso solicitudPermiso) {
		if (solicitudPermiso.getAceptado() == 1) {
			PermisoPersona permisoPersona = new PermisoPersona();
			permisoPersona.setActivo(true);
			permisoPersona.setPersona(solicitudPermiso.getPersona());
			permisoPersona.setVinculoMomentoEspacio(solicitudPermiso.getVinculoMomentoEspacio());
			permisoPersonaRepository.save(permisoPersona);
			solicitudPermiso.setPermisoPersona(permisoPersona);
		}
		solicitudPermisoRepository.save(solicitudPermiso);
	}
	
	public List<SolicitudPermiso> findAllSolicitudPermisos() {
		return (List<SolicitudPermiso>) solicitudPermisoRepository.findAll();
	}
	
	public SolicitudPermiso findById(long id) {
		return solicitudPermisoRepository.findById(id);
	}

	public void updateSolicitudPermiso(SolicitudPermiso solicitudPermiso) {
		solicitudPermisoRepository.save(solicitudPermiso);
	}

	public void deleteSolicitudPermisoById(long id) {
		solicitudPermisoRepository.deleteById(id);
	
	}

	public boolean isSolicitudPermisoExist(SolicitudPermiso solicitudPermiso) {
		return findById(solicitudPermiso.getId())!=null;
	}
	
	public void deleteAllSolicitudPermisos(){
        solicitudPermisoRepository.deleteAll();
	}
	
	public List<SolicitudPermiso> findByEspacio(long idEspacio) {
		System.out.println("Solicitudes para espacio: " + idEspacio);
		return (List<SolicitudPermiso>) solicitudPermisoRepository.findByEspacio(idEspacio);
	}

}