package com.sicce.cda.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.Usuario;
import com.sicce.cda.repository.UsuarioRepository;
import com.sicce.cda.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<Usuario> findAllUsuarios() {
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	public Usuario findById(long id) {
		return usuarioRepository.findById(id);
	}
	
	public Usuario findByNombreUsuario(String usuario) {
		return usuarioRepository.findByNombreUsuario(usuario);
	}
	
	public void saveUsuario(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	public void updateUsuario(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	public void deleteUsuarioById(long id) {
		usuarioRepository.deleteById(id);
	
	}

	public boolean isUsuarioExist(Usuario usuario) {
		return findByNombreUsuario(usuario.getNombreUsuario())!=null;
	}
	
	public void deleteAllUsuarios(){
		usuarioRepository.deleteAll();
}

}