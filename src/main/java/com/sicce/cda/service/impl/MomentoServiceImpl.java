package com.sicce.cda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.Momento;
import com.sicce.cda.repository.MomentoRepository;
import com.sicce.cda.repository.VinculoMomentoEspacioRepository;
import com.sicce.cda.service.MomentoService;

@Service
public class MomentoServiceImpl implements MomentoService {

	private VinculoMomentoEspacioRepository vinculoMomentoEspacioRepository;

	@Autowired
	private MomentoRepository momentoRepository;
	
	@Autowired
    public void setVinculoMomentoEspacioRepository(VinculoMomentoEspacioRepository vinculoMomentoEspacioRepository) {
        this.vinculoMomentoEspacioRepository = vinculoMomentoEspacioRepository;
    }
	
	public Momento findById(long id) {
		return momentoRepository.findById(id);
	}

	public List<Momento> findByEspacio(long id) {
		return (List<Momento>) momentoRepository.findByEspacio(id);
	}

	public void saveMomento(Momento momento) {
		momentoRepository.save(momento);
	}

	public void updateMomento(Momento momento) {
		momentoRepository.save(momento);
	}

	public void deleteMomentoById(long id) {
		momentoRepository.deleteById(id);
	}

	public List<Momento> findAllMomentos() {
		return (List<Momento>) momentoRepository.findAll();
	}

	public void deleteAllMomentos() {
		momentoRepository.deleteAll();
	}

	public boolean isMomentoExist(Momento momento) {
		return findById(momento.getId())!=null;
	}

	public void saveHorarioByEspacio(long idEspacio, Momento momento) {
		momentoRepository.save(momento);
		vinculoMomentoEspacioRepository.saveByEspacio(idEspacio,momento.getId());
	}

}
