package com.sicce.cda.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.Persona;
import com.sicce.cda.repository.PersonaRepository;
import com.sicce.cda.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaRepository personaRepository;
	
	public List<Persona> findAllPersonas() {
		return (List<Persona>) personaRepository.findAll();
	}
	
	public Persona findById(long id) {
		return personaRepository.findById(id);
	}
	
	public Persona findByEmail(String usuario) {
		return personaRepository.findByEmail(usuario);
	}

	public Persona findByMacAddress(String macAddress) {
		return personaRepository.findByMacAddress(macAddress);
	}
	
	public void savePersona(Persona persona) {
		personaRepository.save(persona);
	}

	public void updatePersona(Persona persona) {
		personaRepository.save(persona);
	}

	public void deletePersonaById(long id) {
		personaRepository.deleteById(id);
	
	}

	public boolean isPersonaExist(Persona persona) {
		return findByEmail(persona.getEmail())!=null;
	}
	
	public void deleteAllPersonas(){
		personaRepository.deleteAll();
}

}