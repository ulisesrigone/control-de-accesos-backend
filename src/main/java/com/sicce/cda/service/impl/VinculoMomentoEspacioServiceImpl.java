package com.sicce.cda.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.VinculoMomentoEspacio;
import com.sicce.cda.repository.VinculoMomentoEspacioRepository;
import com.sicce.cda.service.VinculoMomentoEspacioService;

@Service
public class VinculoMomentoEspacioServiceImpl implements VinculoMomentoEspacioService {

	@Autowired
	private VinculoMomentoEspacioRepository vinculoMomentoEspacioRepository;
	
	public List<VinculoMomentoEspacio> findAllVinculoMomentoEspacios() {
		return (List<VinculoMomentoEspacio>) vinculoMomentoEspacioRepository.findAll();
	}
	
	public VinculoMomentoEspacio findById(long id) {
		return vinculoMomentoEspacioRepository.findById(id);
	}
		
	public void saveVinculoMomentoEspacio(VinculoMomentoEspacio vinculoMomentoEspacio) {
		vinculoMomentoEspacioRepository.save(vinculoMomentoEspacio);
	}

	public void updateVinculoMomentoEspacio(VinculoMomentoEspacio vinculoMomentoEspacio) {
		vinculoMomentoEspacioRepository.save(vinculoMomentoEspacio);
	}

	public void deleteVinculoMomentoEspacioById(long id) {
		vinculoMomentoEspacioRepository.deleteById(id);
	
	}

	public boolean isVinculoMomentoEspacioExist(VinculoMomentoEspacio vinculoMomentoEspacio) {
		return findById(vinculoMomentoEspacio.getId())!=null;
	}
	
	public void deleteAllVinculoMomentoEspacios(){
		vinculoMomentoEspacioRepository.deleteAll();
	}

	public List<VinculoMomentoEspacio> findByEspacio(long idEspacio) {
		return (List<VinculoMomentoEspacio>) vinculoMomentoEspacioRepository.findByEspacio(idEspacio);
	}

}