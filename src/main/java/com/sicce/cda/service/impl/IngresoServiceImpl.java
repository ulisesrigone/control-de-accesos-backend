package com.sicce.cda.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.Ingreso;
import com.sicce.cda.repository.IngresoRepository;
import com.sicce.cda.service.EspacioService;
import com.sicce.cda.service.IngresoService;
import com.sicce.cda.service.PersonaService;

@Service
public class IngresoServiceImpl implements IngresoService {

	private EspacioService espacioService;
	private PersonaService personaService;

	@Autowired
	private IngresoRepository ingresoRepository;

	@Autowired
    public void setEspacioService(EspacioService espacioService) {
        this.espacioService = espacioService;
	}
	
	@Autowired
    public void setPersonaService(PersonaService personaService) {
        this.personaService = personaService;
    }
	
	public List<Ingreso> findAllIngresos() {
		return (List<Ingreso>) ingresoRepository.findAll();
	}
	
	public Ingreso findById(long id) {
		return ingresoRepository.findById(id);
	}
	
	public Ingreso saveIngreso(long idEspacio, long idPersona, LocalDateTime fechaIngreso) {
		Ingreso ingreso = new Ingreso();
		ingreso.setEspacio(espacioService.findById(idEspacio));
		ingreso.setPersona(personaService.findById(idPersona));
		ingreso.setFechaIngreso(fechaIngreso);
		return ingresoRepository.save(ingreso);
	}

	public void updateIngreso(Ingreso ingreso) {
		ingresoRepository.save(ingreso);
	}

	public void deleteIngresoById(long id) {
		ingresoRepository.deleteById(id);
	
	}

	public boolean isIngresoExist(Ingreso ingreso) {
		return findById(ingreso.getId())!=null;
	}
	
	public void deleteAllIngresos(){
		ingresoRepository.deleteAll();
	}

	public List<Ingreso> findByEspacio(long idEspacio) {
		return (List<Ingreso>) ingresoRepository.findByEspacio(idEspacio);
	}

}