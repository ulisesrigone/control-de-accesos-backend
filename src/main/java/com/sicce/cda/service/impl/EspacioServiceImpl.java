package com.sicce.cda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sicce.cda.modelo.Espacio;
import com.sicce.cda.repository.EspacioRepository;
import com.sicce.cda.service.EspacioService;

@Service("EspacioService")
public class EspacioServiceImpl implements EspacioService {

	@Autowired
	private EspacioRepository espacioRepository;
	
	public Espacio findById(long id) {
		return espacioRepository.findById(id);
	}

	public Espacio findByNombre(String nombre) {
		return espacioRepository.findByNombre(nombre);
	}

	public void saveEspacio(Espacio espacio) {
		espacioRepository.save(espacio);
	}

	public void updateEspacio(Espacio espacio) {
		espacioRepository.save(espacio);
	}

	public void deleteEspacioById(long id) {
		espacioRepository.deleteById(id);
	}

	public List<Espacio> findAllEspacios() {
		return (List<Espacio>) espacioRepository.findAll();
}

	public void deleteAllEspacios() {
		espacioRepository.deleteAll();
	}

	public boolean isEspacioExist(Espacio espacio) {
		return findByNombre(espacio.getNombre())!=null;
	}

}