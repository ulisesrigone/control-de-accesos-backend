package com.sicce.cda.service;

import java.util.List;
import com.sicce.cda.modelo.Momento;

public interface MomentoService {
	
	Momento findById(long id);
	void saveMomento(Momento momento);
	void updateMomento(Momento momento);
	void deleteMomentoById(long id);
	List<Momento> findAllMomentos();
	void deleteAllMomentos();
	public boolean isMomentoExist(Momento momento);	
	List<Momento> findByEspacio(long id);
	void saveHorarioByEspacio(long idEspacio, Momento momento);
}
