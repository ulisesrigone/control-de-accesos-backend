package com.sicce.cda.service;

import java.util.List;
import com.sicce.cda.modelo.SolicitudPermiso;

public interface SolicitudPermisoService {

	SolicitudPermiso findById(long id);
	void saveSolicitudPermiso(SolicitudPermiso solicitudPermiso);
	void updateSolicitudPermiso(SolicitudPermiso solicitudPermiso);
	void respuestaSolicitudPermiso(SolicitudPermiso solicitudPermiso);
	void deleteSolicitudPermisoById(long id);
	List<SolicitudPermiso> findAllSolicitudPermisos();
	void deleteAllSolicitudPermisos();
	public boolean isSolicitudPermisoExist(SolicitudPermiso solicitudPermiso);
	List<SolicitudPermiso> findByEspacio(long idEspacio);

}