package com.sicce.cda.service;

import java.time.LocalDateTime;
import java.util.List;
import com.sicce.cda.modelo.Ingreso;

public interface IngresoService {
	
	Ingreso findById(long id);	
	public Ingreso saveIngreso(long idEspacio, long idPersona, LocalDateTime fechaIngreso);
	void updateIngreso(Ingreso ingreso);
	void deleteIngresoById(long id);
	List<Ingreso> findAllIngresos();
	void deleteAllIngresos();
	public boolean isIngresoExist(Ingreso ingreso);
	List<Ingreso> findByEspacio(long idEspacio);

}
