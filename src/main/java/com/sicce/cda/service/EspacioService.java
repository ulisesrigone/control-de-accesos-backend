package com.sicce.cda.service;

import java.util.List;
import com.sicce.cda.modelo.Espacio;

public interface EspacioService {
	
	Espacio findById(long id);
	Espacio findByNombre(String nombre);
	void saveEspacio(Espacio espacio);
	void updateEspacio(Espacio espacio);
	void deleteEspacioById(long id);
	List<Espacio> findAllEspacios();
	void deleteAllEspacios();
	public boolean isEspacioExist(Espacio espacio);
}