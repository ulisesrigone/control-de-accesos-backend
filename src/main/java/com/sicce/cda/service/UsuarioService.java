package com.sicce.cda.service;

import java.util.List;
import com.sicce.cda.modelo.Usuario;

public interface UsuarioService {
	
	Usuario findById(long id);	
	Usuario findByNombreUsuario(String nombreUsuario);
	void saveUsuario(Usuario usuario);
	void updateUsuario(Usuario usuario);
	void deleteUsuarioById(long id);
	List<Usuario> findAllUsuarios();
	void deleteAllUsuarios();
	public boolean isUsuarioExist(Usuario usuario);

}
