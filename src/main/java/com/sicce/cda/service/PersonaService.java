package com.sicce.cda.service;

import java.util.List;
import com.sicce.cda.modelo.Persona;

public interface PersonaService {
	
	Persona findById(long id);
	Persona findByEmail(String email);
	Persona findByMacAddress(String macAddress);
	void savePersona(Persona persona);
	void updatePersona(Persona persona);
	void deletePersonaById(long id);
	List<Persona> findAllPersonas();
	void deleteAllPersonas();
	public boolean isPersonaExist(Persona persona);

}