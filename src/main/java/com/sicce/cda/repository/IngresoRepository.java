package com.sicce.cda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import com.sicce.cda.modelo.Ingreso;

public interface IngresoRepository extends JpaRepository<Ingreso, Long>{
	
	Ingreso findById(long id);
	Ingreso findByPersona(long idPersona);

	@Query(value = "select * from ingreso where espacio_id = :espacio_id order by persona_id desc", nativeQuery = true)
	List<Ingreso> findByEspacio(@Param("espacio_id") long idEspacio);

}
