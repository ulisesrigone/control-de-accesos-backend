package com.sicce.cda.repository;

import java.util.List;
import com.sicce.cda.modelo.SolicitudPermiso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SolicitudPermisoRepository extends JpaRepository<SolicitudPermiso, Long> {
    
    SolicitudPermiso findById(long id);
    SolicitudPermiso findByMacAddress(String macAddress);

    @Query(value = "select sp.* from solicitud_permiso sp inner join vinculo_momento_espacio vme on sp.vinculo_momento_espacio_id = vme.id and vme.espacio_id = :espacio_id where sp.aceptado = '0' and sp.permiso_persona_id is null", nativeQuery = true)
	List<SolicitudPermiso> findByEspacio(@Param("espacio_id") long idEspacio);

}