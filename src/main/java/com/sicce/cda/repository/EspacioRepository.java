package com.sicce.cda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sicce.cda.modelo.Espacio;

public interface EspacioRepository extends JpaRepository<Espacio, Long>{
	
	Espacio findByNombre(String nombre);
	Espacio findById(long id);
	Espacio findByDescripcion(String descripcion);

}
