package com.sicce.cda.repository;

import org.springframework.data.repository.CrudRepository;

import com.sicce.cda.modelo.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	
	Usuario findByNombreUsuario(String usuario);
	Usuario findById(long id);
	
}
