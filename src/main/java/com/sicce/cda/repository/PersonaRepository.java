package com.sicce.cda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.sicce.cda.modelo.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Long>{
	
	Persona findById(long id);
	Persona findByEmail(String email);
	
	@Query(value = "select * from persona where mac_address = :macAddress", nativeQuery = true)
	Persona findByMacAddress(@Param("macAddress") String macAddress);
}