package com.sicce.cda.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import com.sicce.cda.modelo.PermisoPersona;

@Repository
public interface PermisoPersonaRepository extends JpaRepository<PermisoPersona, Long> {
	
	PermisoPersona findById(long id);
	PermisoPersona findByPersona (long id);
	@Query(value = "select * from permiso_persona pp inner join vinculo_momento_espacio vme on pp.vinculo_momento_espacio_id = vme.id and vme.espacio_id = :espacio_id", nativeQuery = true)
	List<PermisoPersona> findByEspacio(@Param("espacio_id") long idEspacio);	
}
