package com.sicce.cda.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import javax.transaction.Transactional;
import com.sicce.cda.modelo.VinculoMomentoEspacio;

@Repository
public interface VinculoMomentoEspacioRepository extends JpaRepository<VinculoMomentoEspacio, Long> {
	
	VinculoMomentoEspacio findByMomento(long id);
    VinculoMomentoEspacio findById(long id);
    
    @Modifying
    @Transactional
    @Query(value = "insert into vinculo_momento_espacio (espacio_id, momento_id) values (:idEspacio, :idMomento)", nativeQuery = true)
    void saveByEspacio(@Param("idEspacio") long idEspacio, @Param("idMomento") long idMomento);
    
    @Query(value = "select * from vinculo_momento_espacio where espacio_id = :espacio_id", nativeQuery = true)
	List<VinculoMomentoEspacio> findByEspacio(@Param("espacio_id") long idEspacio);

}