package com.sicce.cda.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;
import com.sicce.cda.modelo.Momento;

public interface MomentoRepository extends CrudRepository<Momento, Long>{
	
	Momento findById(long id);
	
	@Query(value = "select * from momento m inner join vinculo_momento_espacio vme on vme.momento_id = m.id and vme.espacio_id = :espacio_id", nativeQuery = true)
	List<Momento> findByEspacio(@Param("espacio_id") long idEspacio);

	@Query(value = "select m.* from momento m inner join vinculo_momento_espacio vme on vme.momento_id = m.id and vme.espacio_id = :espacio_id inner join permiso_persona pp on pp.vinculo_momento_espacio_id = vme.id and pp.persona_id = :persona_id", nativeQuery = true)
	List<Momento> findByEspacioAndPersona(@Param("espacio_id") long idEspacio, @Param("persona_id") long idPersona);

}
